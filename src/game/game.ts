import { Entity } from '@/utils';

export class Game extends Entity {
  private _lastTimestamp = 0; // game has to calculate the deltaTime used elsewhere to update

  public Update(): void {
    const deltaTime = (Date.now() - this._lastTimestamp) / 1000;

    super.Update(deltaTime); // we pass deltaTime to all game's components using the parents method defined in Entity

    // then update the timestamp
    this._lastTimestamp = Date.now();

    // Invoke on next frame
    window.requestAnimationFrame(() => this.Update())
  }

}
